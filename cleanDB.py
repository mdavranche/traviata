#!/usr/bin/python3

import argparse
import configparser
import logging
import pymysql
import sys


def clean_table(table, where_col, where_val):
    """Clean table where where_col is where_val"""
    if args.debug:
        dbCursor.execute('SELECT * FROM `{0}` WHERE `{1}` = %s'.format(table, where_col), where_val)
        will_be_deleted = dbCursor.fetchall()
        logging.debug('The {0} following row{1} will be deleted on `{2}` table:'.format(len(will_be_deleted), 's' if len(will_be_deleted) > 1 else '', table))
        for row in will_be_deleted:
            logging.debug(row)
    dbCursor.execute('DELETE FROM `{0}` WHERE `{1}` = %s'.format(table, where_col), where_val)
    dbCursor.fetchone()
    logging.debug('Done')


# Get options ; Set emails_real_toclean
parser = argparse.ArgumentParser(description="Clean the erine.email database cleaning following Traviata tests")
parser.add_argument("--emails", action="store", required=True, help="comma-separated list of email addresses to clean")
parser.add_argument("--debug", action="store_true", help="display debug informations")
args = parser.parse_args()
emails_real_toclean = args.emails.split(',')

# Set logging
logging.basicConfig(format='%(asctime)s [%(levelname)-7s] %(message)s', level=logging.DEBUG if args.debug else logging.INFO, stream=sys.stdout)

# Retrieve DBMS configuration and connect to the database
# As we will log-in as root (neither spameater or www can do this cleanup), we
# do not use host and port (root can not connect remotely), but the Unix socket
try:
    config = configparser.RawConfigParser()
    config.read_file(open('/etc/erine-email.conf'))
    database = config.get('dbms', 'database', fallback='spameater')
except Exception as e:
    logging.warning('Problem retrieving DBMS configuration. Falling back to spameater database.')
    database = 'spameater'
try:
    with open('/root/.mariadb.pwd', 'r') as fd:
        password = fd.readline().strip()
    connector = pymysql.connect(unix_socket='/var/run/mysqld/mysqld.sock', connect_timeout=2, user='root', passwd=password, db=database)
    dbCursor = connector.cursor()
except Exception as e:
    logging.error('Exception while connecting to DB: {0}'.format(e))
    exit(1)

# Vacuum the database
for email_real_toclean in emails_real_toclean:
    reply_addresses_count = 0
    messages_count = 0
    print('- Real email to clean: {0}'.format(email_real_toclean))
    try:
        dbCursor.execute('SELECT `disposableMailAddress`.`mailAddress` FROM `disposableMailAddress` INNER JOIN `user` ON `user`.`ID` = `disposableMailAddress`.`userID` WHERE `user`.`mailAddress` = %s', email_real_toclean)
        disposableMailAddresses = dbCursor.fetchall()
        print('  {0} linked disposable mail address{1}{2}'.format(len(disposableMailAddresses), 'es' if len(disposableMailAddresses) > 1 else '', ':' if len(disposableMailAddresses) > 0 else ''))
        for row1 in disposableMailAddresses:
            print('  - {0}'.format(row1[0]))
            dbCursor.execute('SELECT `mailAddress` FROM `replyAddress` WHERE `disposableMailAddress` = %s', row1[0])
            replyAddresses = dbCursor.fetchall()
            print('    {0} linked reply address{1}{2}'.format(len(replyAddresses), 'es' if len(replyAddresses) > 1 else '', ':' if len(replyAddresses) > 0 else ''))
            reply_addresses_count += len(replyAddresses)
            for row2 in replyAddresses:
                print('    - {0}'.format(row2[0]))
            dbCursor.execute('SELECT COUNT(*) FROM `message` WHERE `disposableMailAddress` = %s', row1[0])
            row2 = dbCursor.fetchone()
            print('    {0} linked message{1}'.format(row2[0], 's' if row2[0] > 1 else ''))
            messages_count += row2[0]
    except Exception as e:
        logging.error('Exception while retrieving linked disposable mail addresses: {0}'.format(e))
        exit(1)
    if len(disposableMailAddresses) == 0 and reply_addresses_count == 0 and messages_count == 0:
        print('Nothing to clean')
    else:
        answer = None
        while answer not in ['y', 'Y', 'n', 'N']:
            try:
                answer = input('Clean {0} disposable mail address{1}, {2} reply address{3} and {4} message{5}? [y/n] '.format(len(disposableMailAddresses), 'es' if len(disposableMailAddresses) > 1 else '', reply_addresses_count, 'es' if reply_addresses_count > 1 else '', messages_count, 's' if messages_count > 1 else ''))
            except:  ## The main common case is a Ctrl+C from the user
                exit(1)
        if answer in ['n', 'N']:
            print('Skipped')
            exit(0)
        else:
            for row1 in disposableMailAddresses:
                clean_table('replyAddress', 'disposableMailAddress', row1[0])
                clean_table('message', 'disposableMailAddress', row1[0])
                clean_table('disposableMailAddress', 'mailAddress', row1[0])
            logging.debug('Committing...')
            connector.commit()
            logging.debug('Done')
            print('Done')
connector.close()
