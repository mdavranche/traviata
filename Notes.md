You'll find here notes about specific implementation details of *Traviata*.

# Yahoo Mail does not comply with RFC-3501

## Context

The IMAP `search` command, described in the [RFC-3501](https://tools.ietf.org/html/rfc3501#section-6.4.4), provides the possibility to search mailboxes for messages that match the given searching criteria. You can search for emails sent on a particular date, having a specific IMAP tag, matching a string in their subject, and much more.

To isolate emails of a specific conversation, *Traviata* uses a randomly generated string contained in the subject of the emails. For instance, a *Traviata* email subjects can be: `39c1z4773gzixit - Traviata test`. This is why the IMAP `search` command is essential for us.

## Searching "INBOX"

To show you how the `search` command works, I emptied the mailboxes of my Yahoo Mail account, then sent an email to this account, with a specific and distinctive subject: `xxx li5pnly36s8ztgh yyy`. Then I launched a Python script that makes 2 IMAP searches in the `INBOX` mailbox:
- With the `ALL` search key, searching all emails of the mailbox without distinction,
- With the `SUBJECT` search key, searching for a part of my distinctive subject.

Note that the [RFC](https://tools.ietf.org/html/rfc3501#section-5.1) says that the case-insensitive mailbox name `INBOX` is a special name reserved to mean "the primary mailbox for this user on this server".

```
#!/usr/bin/python3

import email
import imaplib

def list_subjects(status, data):
    if status != 'OK':
        search_status = {
            'OK': 'search completed',
            'NO': 'search error: can\'t search that [CHARSET] or criteria',
            'BAD': 'command unknown or arguments invalid',
        }
        raise Exception('Can not perform IMAP search: {0}'.format(search_status.get(status, 'unknown response from server')))
    if data:
        for x in data[0].split():
            xstatus, xdata = imap_session.fetch(x, '(RFC822)')
            xemail = email.message_from_bytes(xdata[0][1])
            print(xemail['Subject'])

# Initiate IMAP session
imap_session = imaplib.IMAP4_SSL(host='imap.mail.yahoo.com', port=993)
imap_session.login('fake_user', 'fake_password')

# Select mailbox
imap_session.select('"INBOX"')

# Search without criteria
print('All emails:')
status, data = imap_session.search(None, 'ALL')
list_subjects(status, data)

# Search with criteria
print('emails with search criteria:')
status, data = imap_session.search(None, 'SUBJECT', 'li5pnly36s8ztgh')
list_subjects(status, data)

# Close IMAP session
imap_session.close()
imap_session.logout()
```

Also note that the goal of this script is to make an easy, understandable proof of concept. This is why it does not handle errors cases.

The output is consistent, and as expected:

```
All emails:
xxx li5pnly36s8ztgh yyy
emails with search criteria:
xxx li5pnly36s8ztgh yyy
```

## The "Bulk Mail" mailbox

As an email can be moved to the bulk folder, *Traviata* also looks for the messages there. Depending on your email provider, a bulk folder can be named differently: *spam*, *bulk*, *Bulk Mail* or else. The only thing in common between those folders is the `\\Junk` flag. So, to retrieve the name of the bulk folder, *Traviata* asks to the IMAP server the name of the folder having this flag. Yahoo Mail answers "Bulk Mail".

Now, let's move our email to this folder using the *Mark as spam* feature. Then, let's launch our script changing the mailbox name:

```
< imap_session.select('"INBOX"')
---
> imap_session.select('"Bulk Mail"')
```

The output is now inconsistent:

```
All emails:
xxx li5pnly36s8ztgh yyy
emails with search criteria:
```

## Other mailboxes

The previous test had also been made on other mailboxes. Conclusion: the `search` command works everywhere, except in the "Bulk Mail" folder.

## The workaround

As we can not fix the problem (Yahoo Mail should do that), we have to find a workaround, for Yahoo Mail only: retrieving all the emails of the bulk folder, and check them all, on the client side. Obviously, this method is much less efficient than the server side search.
