#!/usr/bin/python3

import argparse
import email
import imaplib
import logging
import os
import random
import re
import smtplib
import string
import sys
import tempfile
import time
import yaml

from email.message import EmailMessage
from io import StringIO


class user():
    imap_session = None
    imap_junk = None
    imap_trash = None

    def __init__(self, config, name, wait_before_send):
        self.config = config
        if name not in ['Alice', 'Bob']:
            raise Exception('Unknown name: {0}'.format(name))
        self.name = name
        self.wait_before_send = wait_before_send

    def login(self):
        if self.imap_session:
            raise Exception('Already logged-in')
        self.imap_session = imap_login(self.config['imap'])
        try:
            self.imap_junk = imap_mailbox_with_flag(self.imap_session, '\\Junk')
        except Exception as e:
            logging.error('Can not retrieve junk mailbox:\n{0}'.format(e))
        try:
            self.imap_trash = imap_mailbox_with_flag(self.imap_session, '\\Trash')
        except Exception as e:
            logging.error('Can not retrieve trash mailbox:\n{0}'.format(e))

    def send_email(self, to_address, message, use_display_names):
        if self.wait_before_send:
            logging.info('Waiting {0} second{1} to prevent ban...'.format(self.wait_before_send, 's' if self.wait_before_send > 1 else ''))
            time.sleep(self.wait_before_send)
            logging.info('Nap finished, back to work!')
        if use_display_names:
            message['From'] = '"{0}" <{1}>'.format(self.name, self.config['email']['real'])
            message['To'] = '"{0}" <{1}>'.format('Alice' if self.name == 'Bob' else 'Bob', to_address)
        else:
            message['From'] = self.config['email']['real']
            message['To'] = to_address
        if not send_email(message, self.config['smtp']):
            logging.error('Failed')
            return False
        logging.info('Message sent')
        return True

    def read_email(self, subject_id, use_display_names, my_address, other_s_address):
        """Read and check the email identified with its subject

        Return:
        - If the email had been successfully retrieved and checked. If False,
          the following parameters are set to None.
        - The email number
        - The whole email
        - The from address
        """
        try:
            email_num, email = imap_fetch(self.imap_session, self.imap_junk, subject_id, self.config['imap']['server'])
            if use_display_names:
                expected_from_display_name = 'Alice - {0}'.format(other_s_address) if self.name == 'Bob' else 'Bob'
                expected_to_display_name = self.name
            else:
                expected_from_display_name = other_s_address if self.name == 'Bob' else ''
                expected_to_display_name = ''
            from_address = get_address(full_address=email['from'], expected_display_name=expected_from_display_name)
            to_address = get_address(full_address=email['To'], expected_display_name=expected_to_display_name)
            if self.name == 'Alice' and from_address != other_s_address:
                logging.error('Wrong address. Found "{0}", expected "{1}".'.format(from_address, other_s_address))
                return(False, None, None, None)
            if to_address != my_address:
                logging.error('Wrong address. Found "{0}", expected "{1}".'.format(to_address, my_address))
                return(False, None, None, None)
        except Exception as e:
            logging.error(e)
            return(False, None, None, None)
        logging.info('email received and checked')
        return(True, email_num, email, from_address)

    def logout(self):
        if not self.imap_session:
            raise Exception('Not logged-in')
        if self.imap_session.state != 'AUTH':
            imap_logout(self.imap_session)
        self.imap_session = None
        self.imap_junk = None
        self.imap_trash = None


def random_string():
    """Return a random string, made of lowercase chars and digits"""
    return(''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(15)))


def check_dictionary(dictionary, label, mandatory_params, optional_params=[]):
    """Return if all params are in dictionary, and vice versa"""
    all_good = True
    for key in dictionary:
        if key not in mandatory_params and key not in optional_params:
            logging.error('Unknown "{0}" parameter on {1}'.format(key, label))
            all_good = False
    for key in mandatory_params:
        if key not in dictionary:
            logging.error('Mandatory "{0}" parameter not found on {1}'.format(key, label))
            all_good = False
    return(all_good)


def log_stderr(string_io):
    """If string_io contains something, log and reset it"""
    if string_io.getvalue():
        logging.debug(string_io.getvalue().rstrip('\n'))
        string_io.close()
        new_string_io = StringIO()
        sys.stderr = new_string_io
        return(new_string_io)
    return(string_io)


def send_email(msg, smtp_config):
    """Send the msg email using the smtp_config SMTP configuration ; Return if sending succeeded"""

    # TODO  - Implement non-SSL or remove ssl parameter
    if not smtp_config['ssl']:
        logging.error('Non-SSL is not yet implemented')
        return False

    try:
        smtp_connection = smtplib.SMTP_SSL(host=smtp_config['server'], port=smtp_config['port'])
        logging.debug('Connected to {0}:{1}'.format(smtp_config['server'], smtp_config['port']))

        # Print SMTP debug informations to stderr
        smtp_connection.set_debuglevel(True)

        # Redirect stderr to the stderr_var variable
        old_stderr = sys.stderr
        stderr_var = StringIO()
        sys.stderr = stderr_var

        # Send msg
        smtp_connection.login(user=smtp_config['username'], password=smtp_config['password'])
        stderr_var = log_stderr(stderr_var)
        logging.debug('Logged-in as {0}'.format(smtp_config['username']))
        smtp_connection.send_message(msg)
        stderr_var = log_stderr(stderr_var)
        logging.debug('Message sent')
        smtp_connection.quit()
        stderr_var = log_stderr(stderr_var)
        logging.debug('Connection closed')

        # Stop redirecting stderr to the stderr_var variable
        sys.stderr = old_stderr

    except Exception as e:
        logging.error(e)
        return False
    return True


def imap_login(imap_config):
    """Initiate an IMAP session"""
    try:
        imap_session = imaplib.IMAP4_SSL(host=imap_config['server'], port=imap_config['port'])
        imap_session.login(imap_config['username'], imap_config['password'])
    except Exception as e:
        raise Exception('Can not connect to {0}:{1} as {2}:\n{3}'.format(imap_config['server'], imap_config['port'], imap_config['username'], e))
    return(imap_session)


def imap_logout(imap_session):
    """Close an IMAP session"""
    imap_session.close()
    imap_session.logout()


def imap_search(imap_session, mailbox, subject_text):
    """Return the list of mailbox's message IDs having subject_text in their subject"""
    logging.debug('Looking in the {0} mailbox (IMAP SELECT)'.format(mailbox))
    imap_session.select(mailbox)
    status, data = imap_session.search(None, 'SUBJECT', subject_text)
    if status != 'OK':
        search_status = {
            'OK': 'search completed',
            'NO': 'search error: can\'t search that [CHARSET] or criteria',
            'BAD': 'command unknown or arguments invalid',
        }
        raise Exception('Can not perform IMAP search: {0}'.format(search_status.get(status, 'unknown response from server')))
    return([x for x in data if x])


def imap_full_search(imap_session, mailbox, subject_text):
    """Return the list of mailbox's message IDs having subject_text in their subject, forcing the ALL type search

    See Notes.md for more details about this function.
    """
    logging.debug('Looking in the {0} mailbox (IMAP SELECT)'.format(mailbox))
    imap_session.select(mailbox)
    status, data = imap_session.search(None, 'ALL')
    if status != 'OK':
        search_status = {
            'OK': 'search completed',
            'NO': 'search error: can\'t search that [CHARSET] or criteria',
            'BAD': 'command unknown or arguments invalid',
        }
        raise Exception('Can not perform IMAP search: {0}'.format(search_status.get(status, 'unknown response from server')))
    data = [x for x in data if x]
    if len(data) == 0:
        return []
    logging.debug('{0} email{1} found in {2} folder:'.format(len(data[0].split()), 's' if len(data[0].split()) > 1 else '', mailbox))
    result = ''
    for x in data[0].split():
        xstatus, xdata = imap_session.fetch(x, '(RFC822)')  ## We asked for only 1 message part (RFC822)
        xemail = email.message_from_bytes(xdata[0][1])
        logging.debug('Message #{0} "{1}"'.format(str(x), xemail['Subject']))
        if subject_text in xemail['Subject']:
            logging.debug('Match')
            result += '{0} '.format(x.decode('UTF-8'))
        else:
            logging.debug('No match')
    return([result] if result else [])


def imap_fetch(imap_session, imap_junk, subject_text, imap_server):
    """Retrieve the email having subject_text in its subject

    The search is performed in the INBOX and imap_junk mailboxes
    """
    for time_wait in [1, 2, 5, 10, 30]:
        for loop in range(5):
            messages = imap_search(imap_session, 'INBOX', subject_text)
            if len(messages) == 0 and imap_junk:
                for mailbox in imap_junk:
                    messages = imap_search(imap_session, mailbox, subject_text)

                    # See Notes.md for more details about this bloc
                    if imap_server == 'imap.mail.yahoo.com' and len(messages) == 0 and loop == 4:
                        logging.debug('email still not found after {0} seconds'.format(loop * time_wait))
                        logging.debug('As we are calling imap.mail.yahoo.com, performing a full search on the {0} mailbox.'.format(mailbox))
                        messages = imap_full_search(imap_session, mailbox, subject_text)

                    if len(messages) > 0:
                        logging.warning('email is marked as junk (found in {0} mailbox)'.format(mailbox))
                        break
            if len(messages) == 0:
                logging.info('email not found, waiting {0} second{1}'.format(time_wait, 's' if time_wait > 1 else ''))
                time.sleep(time_wait)
                continue
            if len(messages[0].split()) > 1:
                raise Exception('{0} messages found, but only 1 was expected'.format(len(messages[0].split())))
            email_num = messages[0].split()[0]
            status, data = imap_session.fetch(email_num, '(RFC822)')
            if status != 'OK':
                fetch_status = {
                    'OK': 'fetch completed',
                    'NO': 'fetch error: can\'t fetch that data',
                    'BAD': 'command unknown or arguments invalid'
                }
                raise Exception('Can not fetch message {0} via IMAP: {1}'.format(email_num, fetch_status.get(status, 'unknown response from server')))
            message_part = data[0]  ## We asked for only 1 message part (RFC822)
            full_email = email.message_from_bytes(message_part[1])
            return(email_num, full_email)
    raise Exception('email not found')


def imap_mailbox_with_flag(imap_session, flag):
    """Return the IMAP mailbox names with a specific flag"""
    logging.debug('Retrieving mailboxes (IMAP LIST)')
    status, data = imap_session.list()
    if status != 'OK':
        list_status = {
            'OK': 'list completed',
            'NO': 'list failure: can\'t list that reference or name',
            'BAD': 'command unknown or arguments invalid'
        }
        raise Exception('Can not list mailboxes via IMAP: {0}'.format(list_status.get(status, 'unknown response from server')))
    list_response_pattern = re.compile(r'\((?P<flags>.*?)\) "(?P<delimiter>.*)" (?P<name>.*)')
    mailboxes = []
    for line in data:
        flags, delimiter, mailbox_name = list_response_pattern.match(line.decode('utf-8')).groups()
        logging.debug('Flags: {0} ; Delimiter: {1} ; Mailbox name: {2}'.format(flags, delimiter, mailbox_name))
        if flag in flags:
            logging.debug('{0} flag found in the {1} mailbox'.format(flag, mailbox_name))
            mailboxes.append(mailbox_name)
    return(mailboxes)


def imap_delete(imap_session, imap_trash, email_num):
    """Delete email"""

    # In some implementations (like GMail), adding the \Deleted flag is not
    # enough. In the GMail case, that only removes the "Inbox" label, and do not
    # add the "Trash" one. This is why we copy it to the trash mailbox.
    if imap_trash and len(imap_trash) > 0:
        imap_session.copy(email_num, imap_trash[0])

    imap_session.store(email_num, '+FLAGS', r'(\Deleted)')
    imap_session.expunge()


def get_address(full_address, expected_display_name):
    """Retrieve the address part of a full address, which should have expected_display_name in its display name part"""
    display_name, address = email.utils.parseaddr(full_address)
    if display_name != expected_display_name:
        raise Exception('Wrong display name. Found "{0}", expected "{1}".'.format(display_name, expected_display_name))

    # Regular expression that addresses must match. You HAVE to prefix this
    # regex by ^ and suffix it by $ to match an address exactly. This regular
    # expression is not intended to match all the addresses, as described by the
    # RFC, but an address handled by erine.email system. This regular expression
    # is also defined in erine.email's spameater.py.
    address_regex = '[_a-z0-9-\+]+(\.[_a-z0-9-\+]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})'

    if not re.match('{0}$'.format(address_regex), address):
        raise Exception('Invalid address found: "{0}".'.format(address))
    return(address)


def check_manually():
    """Ask user to do the action manually, and ask for the result"""
    answer = None
    while answer not in ['y', 'Y', 'n', 'N']:
        try:
            answer = input('Do this manually. Everything was fine with it? [y/n] ')
        except:  ## The main common case is a Ctrl+C from the user
            return(False)
    return answer in ['y', 'Y']


def prefix_reply(text):
    """Prefix every line of text with a greater-than sign"""
    if len(text.split('\n')) != len(text.split('> ')[1:]):

        # Text had never been prefixed: prepend '> '
        return('> {0}'.format('\n> '.join(text.split('\n'))))

    else:

        # Text had already been prefixed: prepend '>'
        return('>{0}'.format('\n> '.join(text.split('\n'))))


def forge_reply(original_email, reply_text):
    """Forge a reply to original_email, without "From" and "To" headers

    reply_text     -- String
    original_email -- Message class instance
    Return         -- EmailMessage class instance
    """
    msg = EmailMessage()
    if 'Subject' in original_email:
        msg['Subject'] = 'Re: {0}'.format(original_email['Subject'])
    else:
        raise Exception('"Subject" header not found')
    if 'message-id' in original_email:

        # Forge In-Reply-To and References according to RFC 2822, 3.6.4.
        msg['In-Reply-To'] = original_email['message-id']
        if 'References' in original_email:
            msg['References'] = '{0} {1}'.format(original_email['References'], original_email['message-id'])
        else:
            msg['References'] = original_email['message-id']

    else:

        raise Exception('"message-id" header not found')
    if 'Thread-Topic' in original_email:
        msg['Thread-Topic'] = original_email['Thread-Topic']
    if 'Thread-Index' in original_email:
        msg['Thread-Index'] = original_email['Thread-Index']
    if original_email.is_multipart():
        raise Exception('The email should not be a multipart message')
    mail_content = prefix_reply(original_email.get_payload().rstrip('\n'))
    mail_content = 'On {0}, {1} wrote:\n\n{2}'.format(original_email['date'], original_email['from'], mail_content)
    mail_content = '{0}\n\n{1}'.format(reply_text, mail_content)
    msg.set_content(mail_content)
    return(msg)


def conversation(use_display_names, buddy1_address, buddy2_address, buddy1, buddy2):
    """Buddy #1 (that can be Alice or Bob) has a conversation with buddy #2
    (Alice if buddy #1 is Bob, and vice-versa). Buddy #1 is the one that
    initiates the conversation. The configuration had already been checked.
    """

    # Part 1
    if buddy1.name == 'Alice':
        logging.info('Part 1: {0} ({1}) is sending message to {2} ({3})...'.format(buddy1.name, buddy1_address, buddy2.name, buddy2_address))
        msg = EmailMessage()
        subject_id = random_string()
        msg['Subject'] = '{0} - Traviata test'.format(subject_id)
        msg.set_content('Hop!')
        if not buddy1.send_email(to_address=buddy2_address, message=msg, use_display_names=use_display_names):
            return False
    else:
        first_shot_address = '{0}.{1}@{2}'.format(buddy1_address.split('@')[0], buddy2_address.replace('@', '_'), buddy1_address.split('@')[1])
        logging.info('Part 1: Bob ({0}) is sending message to Alice ({1}) using a first shot address ({2})...'.format(buddy1_address, buddy2_address, first_shot_address))
        msg = EmailMessage()
        subject_id = random_string()
        msg['Subject'] = '{0} - Traviata test'.format(subject_id)
        msg.set_content('Hop!')
        if not buddy1.send_email(to_address=first_shot_address, message=msg, use_display_names=use_display_names):
            return False

    # Part 2
    logging.info('Part 2: {0} is reading the email from {1}...'.format(buddy2.name, buddy1.name))
    success, email_num, email, buddy1_reply_address = buddy2.read_email(subject_id, use_display_names, buddy2_address, buddy1_address)
    if not success:
        return False
    if buddy1.name == 'Bob':
        for header in email:
            if buddy1.config['email']['real'] in email[header]:
                logging.error('Bob\'s real address ({0}) had been found in Bob email, in the "{1}" header!'.format(buddy1.config['email']['real'], header))
                logging.debug('The email:\n{0}'.format(email))
                return False

    # Part 3
    logging.info('Part 3: {0} is replying to {1}...'.format(buddy2.name, buddy1.name))
    msg = forge_reply(email, 'Hi {0},\nOK, got it!\n{1}'.format(buddy1.name, buddy2.name))
    if not buddy2.send_email(to_address=buddy1_reply_address, message=msg, use_display_names=use_display_names):
        return False

    # Part 4
    logging.info('Part 4: {0} is removing {1}\'s email (that\'s {2} Proper side)...'.format(buddy2.name, buddy1.name, 'his Mr' if buddy2.name == 'Bob' else 'her Mrs'))
    try:
        imap_delete(buddy2.imap_session, buddy2.imap_trash, email_num)
    except Exception as e:
        logging.error(e)
        return False

    # Part 5
    logging.info('Part 5: {0} is reading the email from {1}...'.format(buddy1.name, buddy2.name))
    success, email_num, email, buddy2_reply_address = buddy1.read_email(subject_id, use_display_names, buddy1_address, buddy2_address)
    if not success:
        return False
    if buddy2.name == 'Bob':
        for header in email:
            if buddy2.config['email']['real'] in email[header]:
                logging.error('Bob\'s real address ({0}) had been found in Bob email, in the "{1}" header!'.format(buddy2.config['email']['real'], header))
                logging.debug('The email:\n{0}'.format(email))
                return False

    # Part 6
    logging.info('Part 6: {0} is replying to {1}...'.format(buddy1.name, buddy2.name))
    msg = forge_reply(email, 'Me too, my dear!\n{0}'.format(buddy1.name))
    if not buddy1.send_email(to_address=buddy2_reply_address, message=msg, use_display_names=use_display_names):
        return False

    # Part 7
    logging.info('Part 7: {0} is removing {1}\'s email (that\'s {2} Proper side)...'.format(buddy1.name, buddy2.name, 'his Mr' if buddy2.name == 'Bob' else 'her Mrs'))
    try:
        imap_delete(buddy1.imap_session, buddy1.imap_trash, email_num)
    except Exception as e:
        logging.error(e)
        return False

    # Part 8
    logging.info('Part 8: {0} is reading the email from {1}...'.format(buddy2.name, buddy1.name))
    success, email_num, email, buddy1_reply_address = buddy2.read_email(subject_id, use_display_names, buddy2_address, buddy1_address)
    if not success:
        return False

    # Part 9
    logging.info('Part 9: {0} is removing {1}\'s email (that\'s {2} Proper side, again)...'.format(buddy2.name, buddy1.name, 'his Mr' if buddy2.name == 'Bob' else 'her Mrs'))
    try:
        imap_delete(buddy2.imap_session, buddy2.imap_trash, email_num)
    except Exception as e:
        logging.error(e)
        return False

    return True


def main(argv):

    # Get options
    parser = argparse.ArgumentParser(description="Test the whole erine.email chain, running scenarios where Alice (a non-erine.email user) and Bob (an erine.email user) have conversations")
    parser.add_argument("--configfile", action="store", default="/etc/traviata.conf", help="configuration file")
    parser.add_argument("--keepdebug", action="store_true", help="keep debug file even if everything went well")
    parser.add_argument("--alice", action="store", required=True, help="Alice's configuration section")
    parser.add_argument("--alicewait", action="store", default=0, type=int, help="Alice's waiting time before sending an email, in seconds")
    parser.add_argument("--bob", action="store", required=True, help="Bob's configuration section")
    parser.add_argument("--bobwait", action="store", default=0, type=int, help="Bob's waiting time before sending an email, in seconds")
    parser.add_argument("--features", action="store", default="classic,reserved_address,first_shot", help="comma separated features to test")
    args = parser.parse_args()

    # Set logging
    logging.root.setLevel(logging.DEBUG)
    log_format = logging.Formatter('%(asctime)s [%(levelname)-7s] %(message)s')
    console = logging.StreamHandler(sys.stdout)
    console.setFormatter(log_format)
    console.setLevel(logging.INFO)
    logging.root.addHandler(console)
    debug_file = tempfile.NamedTemporaryFile(prefix='traviata_', suffix='.log', delete=False)
    debug_file.close()
    file_handler = logging.FileHandler(debug_file.name)
    file_handler.setFormatter(log_format)
    file_handler.setLevel(logging.DEBUG)
    logging.root.addHandler(file_handler)
    logging.info('Debug log: {0}'.format(debug_file.name))
    keep_debug = args.keepdebug  # If everything went well, remove debug file except if user asked to keep it

    # Check configuration
    supported_features = ['classic', 'reserved_address', 'first_shot']
    for feature in args.features.split(','):
        if feature not in supported_features:
            logging.error('Unknown feature: {0}'.format(feature))
            logging.error('Supported features: {0}'.format(', '.join(supported_features)))
            exit(1)
    try:
        with open(args.configfile, 'r') as ymlfile:
            config = yaml.load(ymlfile, Loader=yaml.BaseLoader)
    except Exception as e:
        logging.error(e)
        exit(1)
    correct_config = True
    for section in config:
        correct_config = check_dictionary(config[section], section, ['provider', 'email', 'smtp', 'imap']) and correct_config
        if 'email' in config[section]:
            correct_config = check_dictionary(config[section]['email'], '{0} > email'.format(section), ['real'], ['mask_classic', 'mask_reserved']) and correct_config
        if 'smtp' in config[section]:
            correct_config = check_dictionary(config[section]['smtp'], '{0} > smtp'.format(section), ['server', 'port', 'ssl', 'username', 'password']) and correct_config
        if 'imap' in config[section]:
            correct_config = check_dictionary(config[section]['imap'], '{0} > imap'.format(section), ['server', 'port', 'ssl', 'username', 'password']) and correct_config
    if not correct_config:
        logging.error('Aborting ; Please fix configuration first')
        exit(1)
    if args.alice not in config:
        logging.error("Alice's configuration section ({0}) does not exist on {1}".format(args.alice, args.configfile))
        exit(1)
    if args.bob not in config:
        logging.error("Bob's configuration section ({0}) does not exist on {1}".format(args.bob, args.configfile))
        exit(1)

    # Instantiate alice and bob
    alice = user(config[args.alice], 'Alice', args.alicewait)
    bob = user(config[args.bob], 'Bob', args.bobwait)
    if not 'mask_classic' in bob.config['email'] and not 'mask_reserved' in bob.config['email']:
        logging.error('Bob must have a "classic" and/or a "reserved" masked address')
        exit(1)
    if alice.config['email']['real'] == bob.config['email']['real']:
        logging.error("Alice and Bob can't have the same address ({0})".format(alice.config['email']['real']))
        exit(1)
    if alice.config['imap']['username'] == bob.config['imap']['username']:
        logging.error("Alice and Bob can't have the same IMAP account ({0})".format(alice.config['imap']['username']))
        exit(1)

    # Clean the database
    print('Before proceeding, you should clean the database on your server providing the erine.email service launching this on it, as root:')
    print('cleanDB.py --emails {0},{1}'.format(alice.config['email']['real'], bob.config['email']['real']))
    answer = None
    while answer not in ['y', 'Y', 'n', 'N']:
        try:
            answer = input('Continue? [y/n] ')
        except:  ## The main common case is a Ctrl+C from the user
            exit(1)
    if answer in ['n', 'N']:
        logging.error('Abort')
        exit(1)

    # Initiate Alice and Bob IMAP sessions
    alice.login()
    bob.login()

    # Scenarios 1 and 2, classic mask address
    if 'classic' in args.features.split(','):
        if 'mask_classic' not in bob.config['email']:
            logging.warning('Bob does not have "classic" masked email address (he definitely should have one) ; Scenarios 1 and 2 skipped')
        else:
            logging.info('Scenario 1: Alice initiates a conversation with Bob, using his never-used "classic" mask address')
            logging.info('            Both Alice and Bob use display names to identify their addresses')
            bob_address = '{0}.{1}'.format(random_string(), bob.config['email']['mask_classic'])
            conversation_succeeded = conversation(use_display_names=True, buddy1_address=alice.config['email']['real'], buddy2_address=bob_address, buddy1=alice, buddy2=bob)
            if conversation_succeeded:
                logging.info('Scenario 1 succeeded')
            else:
                logging.error('Scenario 1 failed. See {0} for more informations.'.format(debug_file.name))
                keep_debug = True
            logging.info('Scenario 2: Alice initiates a conversation with Bob, using his already-used "classic" mask address')
            logging.info('            Both Alice and Bob do not use display names to identify their addresses')
            if conversation_succeeded:
                if conversation(use_display_names=False, buddy1_address=alice.config['email']['real'], buddy2_address=bob_address, buddy1=alice, buddy2=bob):
                    logging.info('Scenario 2 succeeded')
                else:
                    logging.error('Scenario 2 failed. See {0} for more informations.'.format(debug_file.name))
                    keep_debug = True
            else:
                logging.warning('Scenario 2 skipped (as scenario 1 failed)')

    # Scenarios 3 and 4, reserved mask address
    if 'reserved_address' in args.features.split(','):
        if 'mask_reserved' not in bob.config['email']:
            logging.warning('Bob does not have "reserved" masked address (he definitely should have one) ; Scenarios 3 and 4 skipped')
        else:
            logging.info('Scenario 3: Alice initiates a conversation with Bob, using his never-used "reserved" mask address')
            logging.info('            Both Alice and Bob do not use display names to identify their addresses')
            bob_address = bob.config['email']['mask_reserved']
            conversation_succeeded = conversation(use_display_names=False, buddy1_address=alice.config['email']['real'], buddy2_address=bob_address, buddy1=alice, buddy2=bob)
            if conversation_succeeded:
                logging.info('Scenario 3 succeeded')
            else:
                logging.error('Scenario 3 failed. See {0} for more informations.'.format(debug_file.name))
                keep_debug = True
            logging.info('Scenario 4: Alice initiates a conversation with Bob, using his already-used "reserved" mask address')
            logging.info('            Both Alice and Bob use display names to identify their addresses')
            if conversation_succeeded:
                if conversation(use_display_names=True, buddy1_address=alice.config['email']['real'], buddy2_address=bob_address, buddy1=alice, buddy2=bob):
                    logging.info('Scenario 4 succeeded')
                else:
                    logging.error('Scenario 4 failed. See {0} for more informations.'.format(debug_file.name))
                    keep_debug = True
            else:
                logging.warning('Scenario 4 skipped (as scenario 3 failed)')

    # Scenarios 5 and 6, first shot feature
    if 'first_shot' in args.features.split(','):
        if 'mask_classic' not in bob.config['email']:
            logging.warning('Bob does not have "classic" masked address (he definitely should have one) ; Scenarios 5 and 6 skipped')
        else:
            logging.info('Scenario 5: Bob initiates a conversation with Alice, using his never-used "classic" mask address and the first shot feature')
            logging.info('            Both Alice and Bob use display names to identify their addresses')
            bob_address = '{0}.{1}'.format(random_string(), bob.config['email']['mask_classic'])
            conversation_succeeded = conversation(use_display_names=True, buddy1_address=bob_address, buddy2_address=alice.config['email']['real'], buddy1=bob, buddy2=alice)
            if conversation_succeeded:
                logging.info('Scenario 5 succeeded')
            else:
                logging.error('Scenario 5 failed. See {0} for more informations.'.format(debug_file.name))
                keep_debug = True
            logging.info('Scenario 6: Bob initiates a conversation with Alice, using his already-used "classic" mask address and the first shot feature')
            logging.info('            Both Alice and Bob do not use display names to identify their addresses')
            if conversation_succeeded:
                if conversation(use_display_names=False, buddy1_address=bob_address, buddy2_address=alice.config['email']['real'], buddy1=bob, buddy2=alice):
                    logging.info('Scenario 6 succeeded')
                else:
                    logging.error('Scenario 6 failed. See {0} for more informations.'.format(debug_file.name))
                    keep_debug = True
            else:
                logging.warning('Scenario 6 skipped (as scenario 5 failed)')

    # Close Alice and Bob IMAP sessions
    alice.logout()
    bob.logout()

    # If everything went well, remove debug file except if user asked to keep it
    if not keep_debug:
        os.remove(debug_file.name)


if __name__ == '__main__':
    try:
        main(sys.argv[1:])
    except Exception as e:
        print('ERROR: {0}'.format(e))
        sys.exit(1)
