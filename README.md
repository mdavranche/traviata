# Traviata

*Traviata* is a non-regression test suite for erine.email. It tests the whole erine.email chain, running scenarios where Alice (a non-erine.email user) and Bob (an erine.email user) have a conversation.

## Requirements

For the tests to be concluant, it is recommanded that you use accounts dedicated to Traviata. Those accounts informations have to be set in the Traviata configuration file (see section below).

To clean the database after performing the tests, you will need a root access to your server providing the erine.email service.

## Configuration file

The Traviata configuration file's format is YAML. Here is an example:

```YAML
YahooMail:
  provider: Yahoo Mail
  email:
    real: example@yahoo.com
    # mask_classic will be prefixed with "something." by Traviata
    # mask_classic: Not set
    # mask_reserved: Not set
  smtp:
    server: smtp.mail.yahoo.com
    port: 465
    ssl: True
    username: example@yahoo.com
    password: 61oj5wucKalMchjfOVdG7dW9MdagXP
  imap:
    server: imap.mail.yahoo.com
    port: 993
    ssl: True
    username: example@yahoo.com
    password: 61oj5wucKalMchjfOVdG7dW9MdagXP

GMail:
  provider: GMail
  email:
    real: example@gmail.com
    # mask_classic will be prefixed with "something." by Traviata
    mask_classic: traviata_google@erine.email
    # mask_reserved: Not set
  smtp:
    server: smtp.gmail.com
    port: 465
    ssl: True
    username: example@gmail.com
    password: N95oVpmaE1VkEVgy9aOyM39tkhiTIH
  imap:
    server: imap.gmail.com
    port: 993
    ssl: True
    username: example@gmail.com
    password: N95oVpmaE1VkEVgy9aOyM39tkhiTIH
```

Note that this example is a real one, except for the `real`, `mask_classic`, `username` and `password` values, that are fake.

## Usage

Just tell Traviata who is Alice, who is Bob and, if not in `/etc/traviata.conf`, where is your configuration file:

```bash
./traviata.py --configfile ~/traviata.conf --alice YahooMail --bob GMail
```

If you want to clean your database after the tests, run this as root on your server providing the erine.email service:
```bash
./cleanDB --emails example@yahoo.com,example@gmail.com
- Real email to clean: example@yahoo.com
  0 linked disposable mail address
Nothing to clean
- Real email to clean: example@gmail.com
  1 linked disposable mail address:
  - fjorujar7m6l0ut.traviata_google@erine.email
    1 linked reply address:
    - ydz0ul9vc1h4plt@plops.ovh
    4 linked messages
Clean 1 disposable mail address, 1 reply address and 4 messages? [y/n] y
Done
```

## Note for GMail users

On GMail, IMAP is disabled by default. Do not forget to [enable it](https://support.google.com/mail/answer/7126229?hl=en).

You also have to turn on ["Allow less secure apps"](https://support.google.com/accounts/answer/6010255) in your "Less secure apps" section of your Google account if you want to use Traviata with this account.

GMail allows the usage of the `+` sign. For instance, sending an email to `xxx+something@gmail.com` is the same as sending to `xxx@gmail.com`. If you use an email with the `+` sign as Alice or Bob's real email address, don't forget to setup this address in GMail [Accounts and Import](https://mail.google.com/mail/u/0/#settings/accounts) options. For instance:

![Send mail as](README_send_mail_as.png)

SMTP and IMAP server settings are [here](https://support.google.com/mail/answer/7126229?hl=en).

## Note for Yahoo Mail users

On Yahoo Mail, you have to ["generate a third-party app password"](https://help.yahoo.com/kb/account/learn-generate-third-party-passwords-sln15241.html) in your "Account security" settings if you want to use Traviata with this account.

SMTP and IMAP server settings are [here](https://help.yahoo.com/kb/SLN4075.html).
